package com.emids.insurance;

import com.emids.prop.HabitsType;

public interface InsuranceCalcBasedOnHabits {

	public int basedOnHabits(HabitsType habitsType);

}
