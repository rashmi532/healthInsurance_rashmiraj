package com.emids.insurance.impl;

import com.emids.insurance.InsuranceCalcBasedOnCurrentHealth;
import com.emids.prop.CurrentHealthType;

public class PremiumCalcBasedOnCurrentHealth implements InsuranceCalcBasedOnCurrentHealth{
	
	public int basedOnCurrentHealth(CurrentHealthType currHealth) {
		int percentage = 0;
		
		switch(currHealth) {
		
			case HYPER_TENSION : 
				percentage = 1;
				break;
			case BLOOD_PRESSURE:
				percentage = 1;
				break;
			case BLOOD_SUGAR :
				percentage = 1;
				break;
			case OVER_WEIGHT: 
				percentage = 1;
				break;
				
				default : 
					percentage = 0;
		}
		
		return percentage;
	}

}
