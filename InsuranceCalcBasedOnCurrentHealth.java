package com.emids.insurance;

import com.emids.prop.CurrentHealthType;

public interface InsuranceCalcBasedOnCurrentHealth {

	public int basedOnCurrentHealth(CurrentHealthType currentHealthType);

}
