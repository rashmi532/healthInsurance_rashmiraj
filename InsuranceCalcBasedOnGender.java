package com.emids.insurance;

import com.emids.model.Customer;

public interface InsuranceCalcBasedOnGender {

	public int basedOnGender(Customer cust);

}
