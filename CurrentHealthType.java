package com.emids.prop;

public enum CurrentHealthType {
	
	HYPER_TENSION, BLOOD_PRESSURE, BLOOD_SUGAR, OVER_WEIGHT;

}
