package com.emids.insurance.impl;

import com.emids.insurance.InsuranceCalcBasedOnAge;
import com.emids.model.Customer;

public class PremiumCalcBasedOnAge implements InsuranceCalcBasedOnAge{
	
	private double basePremium = 5000;
	
	public double basedOnAge(Customer cust) {
		
		int percentage = 0;
		double amount = basePremium;
		
		if(cust.getAge() < 18) {
			amount = basePremium;
		}
		else if(cust.getAge() >= 18 && cust.getAge() < 25) {
			 amount = amount + ((amount * 10)/100);
		}
			
		if(cust.getAge() < 40 && cust.getAge() > 18) {
			for(int i=25; i<40; i=i+5) {
				percentage = 10;
				amount = amount + ((amount * percentage)/100);
			}
		}
		else if(cust.getAge() >= 40) {
			for(int i=25; i<40; i=i+5) {
				percentage = 10;
				amount = amount + ((amount * percentage)/100);
			}
			for(int i=40 ; i<cust.getAge() ; i=i+5) {
				percentage = 20;
				amount = amount + ((amount * percentage)/100);
			}
		}
		
		
		return amount ;
		
	}

}
