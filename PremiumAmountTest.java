package com.emids.insurance.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.insurance.InsuranceCalcBasedOnAge;
import com.emids.insurance.InsuranceCalcBasedOnCurrentHealth;
import com.emids.insurance.InsuranceCalcBasedOnGender;
import com.emids.insurance.InsuranceCalcBasedOnHabits;
import com.emids.model.CurrentHealth;
import com.emids.model.Customer;
import com.emids.model.Habits;

public class PremiumAmountTest {

	@Test
	public void testGetPremiumAmountBasedOnAge() {
		CurrentHealth currHealth = new CurrentHealth();
		
		currHealth.setHyperTension(false);
		currHealth.setBloodPressure(false);
		currHealth.setBloodSugar(false);
		currHealth.setOverWeight(true);
		
		Habits habits = new Habits();
		
		habits.setSmoking(false);
		habits.setAlcohal(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		
		Customer cust = new Customer();
		cust.setId(1);
		cust.setName("Norman Gomes");
		cust.setAge(34);
		cust.setGender("M");
		cust.setHabits(habits);
		cust.setCurrentHealth(currHealth);
		
		
		InsuranceCalcBasedOnAge pcBasedOnAge = new PremiumCalcBasedOnAge();
		InsuranceCalcBasedOnGender pcBasedOnGender = new PremiumCalcBasedOnGender();
		InsuranceCalcBasedOnCurrentHealth pcBasedOnCH = new PremiumCalcBasedOnCurrentHealth();
		InsuranceCalcBasedOnHabits pcBasedOnHabits = new PremiumCalcBasedOnHabits();
		
		PremiumAmount premium = new PremiumAmount(cust, pcBasedOnAge, pcBasedOnGender, pcBasedOnCH, pcBasedOnHabits);
		
		double amount = premium.getPremiumAmountBasedOnAge();
		assertEquals(6655.0, amount,0.0);
	}

	@Test
	public void testGetPremiumAmountBasedOnGender() {
		CurrentHealth currHealth = new CurrentHealth();
		
		currHealth.setHyperTension(false);
		currHealth.setBloodPressure(false);
		currHealth.setBloodSugar(false);
		currHealth.setOverWeight(true);
		
		Habits habits = new Habits();
		
		habits.setSmoking(false);
		habits.setAlcohal(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		
		Customer cust = new Customer();
		cust.setId(1);
		cust.setName("Norman Gomes");
		cust.setAge(34);
		cust.setGender("M");
		cust.setHabits(habits);
		cust.setCurrentHealth(currHealth);
		
		
		InsuranceCalcBasedOnAge pcBasedOnAge = new PremiumCalcBasedOnAge();
		InsuranceCalcBasedOnGender pcBasedOnGender = new PremiumCalcBasedOnGender();
		InsuranceCalcBasedOnCurrentHealth pcBasedOnCH = new PremiumCalcBasedOnCurrentHealth();
		InsuranceCalcBasedOnHabits pcBasedOnHabits = new PremiumCalcBasedOnHabits();
		
		PremiumAmount premium = new PremiumAmount(cust, pcBasedOnAge, pcBasedOnGender, pcBasedOnCH, pcBasedOnHabits);
		
		double amount = premium.getPremiumAmountBasedOnAge();
		amount = premium.getPremiumAmountBasedOnGender();
		assertEquals(6788.1, amount,0.0);
	}

	@Test
	public void testGetPremiumAmountBasedOnCurrentHealth() {
		CurrentHealth currHealth = new CurrentHealth();
		
		currHealth.setHyperTension(false);
		currHealth.setBloodPressure(false);
		currHealth.setBloodSugar(false);
		currHealth.setOverWeight(true);
		
		Habits habits = new Habits();
		
		habits.setSmoking(false);
		habits.setAlcohal(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		
		Customer cust = new Customer();
		cust.setId(1);
		cust.setName("Norman Gomes");
		cust.setAge(34);
		cust.setGender("M");
		cust.setHabits(habits);
		cust.setCurrentHealth(currHealth);
		
		InsuranceCalcBasedOnAge pcBasedOnAge = new PremiumCalcBasedOnAge();
		InsuranceCalcBasedOnGender pcBasedOnGender = new PremiumCalcBasedOnGender();
		InsuranceCalcBasedOnCurrentHealth pcBasedOnCH = new PremiumCalcBasedOnCurrentHealth();
		InsuranceCalcBasedOnHabits pcBasedOnHabits = new PremiumCalcBasedOnHabits();
		
		PremiumAmount premium = new PremiumAmount(cust, pcBasedOnAge, pcBasedOnGender, pcBasedOnCH, pcBasedOnHabits);
		
		double amount = premium.getPremiumAmountBasedOnAge();
		amount = premium.getPremiumAmountBasedOnGender();
		amount = premium.getPremiumAmountBasedOnCurrentHealth();

		assertEquals(6856, Math.ceil(amount),0.0);
	}
	
	@Test
	public void testGetPremiumBasedOnHabits() {
		CurrentHealth currHealth = new CurrentHealth();
		
		currHealth.setHyperTension(false);
		currHealth.setBloodPressure(false);
		currHealth.setBloodSugar(false);
		currHealth.setOverWeight(true);
		
		Habits habits = new Habits();
		
		habits.setSmoking(false);
		habits.setAlcohal(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		
		Customer cust = new Customer();
		cust.setId(1);
		cust.setName("Norman Gomes");
		cust.setAge(34);
		cust.setGender("M");
		cust.setHabits(habits);
		cust.setCurrentHealth(currHealth);
		
		InsuranceCalcBasedOnAge pcBasedOnAge = new PremiumCalcBasedOnAge();
		InsuranceCalcBasedOnGender pcBasedOnGender = new PremiumCalcBasedOnGender();
		InsuranceCalcBasedOnCurrentHealth pcBasedOnCH = new PremiumCalcBasedOnCurrentHealth();
		InsuranceCalcBasedOnHabits pcBasedOnHabits = new PremiumCalcBasedOnHabits();
		
		PremiumAmount premium = new PremiumAmount(cust, pcBasedOnAge, pcBasedOnGender, pcBasedOnCH, pcBasedOnHabits);
		
		double amount = premium.getPremiumAmountBasedOnAge();
		amount = premium.getPremiumAmountBasedOnGender();
		amount = premium.getPremiumAmountBasedOnCurrentHealth();
		amount = premium.getPremiumAmountBasedOnHabits();
		assertEquals(6856, Math.ceil(amount),0.0);
	}

}
