package com.emids.insurance.impl;

import com.emids.insurance.InsuranceCalcBasedOnGender;
import com.emids.model.Customer;

public class PremiumCalcBasedOnGender implements InsuranceCalcBasedOnGender{
	
	public int basedOnGender(Customer cust) {
		int percentage = 0;
		if(cust.getGender().equals("M")) {
			percentage = 2;
		} 
		return percentage;
	}


}
