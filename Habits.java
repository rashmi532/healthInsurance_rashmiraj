package com.emids.model;

public class Habits {
	
	private boolean smoking;
	private boolean alcohal;
	private boolean dailyExercise;
	private boolean drugs;
	
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohal() {
		return alcohal;
	}
	public void setAlcohal(boolean alcohal) {
		this.alcohal = alcohal;
	}
	public boolean isDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
	

}
