package com.emids.insurance.impl;

import com.emids.insurance.InsuranceCalcBasedOnAge;
import com.emids.insurance.InsuranceCalcBasedOnCurrentHealth;
import com.emids.insurance.InsuranceCalcBasedOnGender;
import com.emids.insurance.InsuranceCalcBasedOnHabits;
import com.emids.model.Customer;
import com.emids.prop.CurrentHealthType;
import com.emids.prop.HabitsType;

public class PremiumAmount {
	
	private Customer customer;
	private double basePremium = 5000;
	private double totalPremium = 0;
	private InsuranceCalcBasedOnAge premCalcBasedOnAge ;
	private InsuranceCalcBasedOnGender premCalcBasedOnGender ;
	private InsuranceCalcBasedOnCurrentHealth premCalcBasedOnCH ;
	private InsuranceCalcBasedOnHabits premCalcBasedOnHabits ;
	
	public PremiumAmount(Customer cust, InsuranceCalcBasedOnAge age, InsuranceCalcBasedOnGender gender,
			InsuranceCalcBasedOnCurrentHealth ch, InsuranceCalcBasedOnHabits habits) {
		this.customer = cust;
		this.premCalcBasedOnAge = age;
		this.premCalcBasedOnGender = gender;
		this.premCalcBasedOnCH = ch;
		this.premCalcBasedOnHabits = habits;
	}

	public double getPremiumAmountBasedOnAge() {
		totalPremium = basePremium;
		totalPremium = premCalcBasedOnAge.basedOnAge(customer); 
		
		return totalPremium;
	}
	
	public double getPremiumAmountBasedOnGender() {

		//based on gender
		totalPremium = totalPremium + ((totalPremium * premCalcBasedOnGender.basedOnGender(customer))/100);
		
		return totalPremium;
	}
	
	public double getPremiumAmountBasedOnHabits() {
		int totalPercentage = 0;
		
			//based on habits
			
			if(customer.getHabits().isSmoking()) 
				totalPercentage =  premCalcBasedOnHabits.basedOnHabits(HabitsType.SMOKING);
			if(customer.getHabits().isAlcohal()) 
				totalPercentage = totalPercentage + premCalcBasedOnHabits.basedOnHabits(HabitsType.ALCOHOL);
			if(customer.getHabits().isDailyExercise()) 
				totalPercentage = totalPercentage - premCalcBasedOnHabits.basedOnHabits(HabitsType.DAILY_EXERCISE);
			if(customer.getHabits().isDrugs()) 
				totalPercentage = totalPercentage + premCalcBasedOnHabits.basedOnHabits(HabitsType.DAILY_EXERCISE);
				
			
			totalPremium = totalPremium + ((totalPremium * totalPercentage)/100);

		return totalPremium;
	}
	
	public double getPremiumAmountBasedOnCurrentHealth() {
		
		//based on currentHealth
		if(customer.getCurrentHealth().isHyperTension()) 
			totalPremium = totalPremium + ((totalPremium * premCalcBasedOnCH.basedOnCurrentHealth(CurrentHealthType.HYPER_TENSION))/100);
		if(customer.getCurrentHealth().isBloodPressure()) 
			totalPremium = totalPremium + ((totalPremium * premCalcBasedOnCH.basedOnCurrentHealth(CurrentHealthType.BLOOD_PRESSURE))/100);
		if(customer.getCurrentHealth().isBloodSugar()) 
			totalPremium = totalPremium + ((totalPremium * premCalcBasedOnCH.basedOnCurrentHealth(CurrentHealthType.BLOOD_SUGAR))/100);
		if(customer.getCurrentHealth().isOverWeight()) 
			totalPremium = totalPremium + ((totalPremium * premCalcBasedOnCH.basedOnCurrentHealth(CurrentHealthType.OVER_WEIGHT))/100);
		
		return totalPremium;
	}	
	

}
