package com.emids.insurance.impl;

import com.emids.insurance.InsuranceCalcBasedOnHabits;
import com.emids.prop.HabitsType;

public class PremiumCalcBasedOnHabits implements InsuranceCalcBasedOnHabits{
	
	public int basedOnHabits(HabitsType habitsType) {
		
		int percentage = 0;
		
		switch(habitsType) {
			case SMOKING :
				percentage = 3;
				break;
			case ALCOHOL : 
				percentage = 3;
				break;
			case DAILY_EXERCISE :
				percentage = 3;
				break;
			case DRUGS :
				percentage = 3;
				break;
			
			default : 
				percentage = 0;
		}
		return percentage;
	}


}
